This module provides a TouchNet uPay payment method for Drupal Commerce.

Requirements
-----------------------------------------
1. An already-configured TouchNet uPay site
   a. The site must have a UPAY_SITE_ID set.
   b. The site must have a status of ONLINE
   c. The site must have a Posting URL (this module will
      tell you what the value should be set to; you will
	  see this value at the top of the action configuration
	  page (see Configuration section below).

2. HTTPS - TouchNet must open their firewall in order to pass data
   back to this Drupal site. In my experience they will not
   open up to port 80. They will, however, open port 443 (HTTPS/SSL),
   so please make sure your site can support this.

Installation
-----------------------------------------
1. Enable the module (and dependencies).
2. Go to Store > Configuration > Payment methods and enable the
   TouchNet uPay payment method rule.

Configuration
-----------------------------------------
1. Go to Store > Configuration > Payment methods and click the
   "edit" operation for the TouchNet uPay payment method rule.
2. Click the "edit" operation for the "Enable payment method:
   TouchNet uPay" action.
3. Fill out all required fields
